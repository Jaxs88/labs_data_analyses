from airflow.decorators import dag, task

from datetime import date
from datetime import datetime
from datetime import timedelta

import boto3
import json
import base64
import re
import requests


def get_secret(secret_name):
    region_name = "us-east-1"

    session = boto3.session.Session()
    client = session.client(
        service_name='secretsmanager',
        region_name=region_name
    )

    get_secret_value_response = client.get_secret_value(SecretId=secret_name)
    if 'SecretString' in get_secret_value_response:
        return get_secret_value_response['SecretString']
    else:
        return base64.b64decode(get_secret_value_response['SecretBinary'])


API_URL = 'https://api.crossref.org'
MAILTO = get_secret('sampling-framework-mailto')

CODE_BUCKET = get_secret('bucket-name-emr-code')
BOOTSTRAP_SCRIPT = f's3://{CODE_BUCKET}/resolution-logs/bootstrap.sh'
PREPARE_LOGS_SCRIPT = f's3://{CODE_BUCKET}/resolution-logs/prepare-logs.py'
MEMBER_COUNTS_SCRIPT = f's3://{CODE_BUCKET}/resolution-logs/member-counts.py'
MEMBER_BREAKDOWN_SCRIPT = f's3://{CODE_BUCKET}/resolution-logs/member-breakdown.py'
OVERALL_STATISTICS_SCRIPT = f's3://{CODE_BUCKET}/resolution-logs/overall-statistics.py'

DATE = '2023-05'
INPUT_BUCKET = get_secret('bucket-name-outputs-private')
LOGS_INPUT = f's3://{INPUT_BUCKET}/resolution-logs/{DATE}/CrossRef-access.log*'
PREFIXES_KEY = 'resolution-logs/prefix-map.json'
PREFIXES = f's3://{INPUT_BUCKET}/{PREFIXES_KEY}'
LOGS_MEMBER_IDS = f's3://{INPUT_BUCKET}/resolution-logs/tmp/{DATE}'

OUTPUT_BUCKET = get_secret('bucket-name-outputs')
OUTPUT_PREFIX = 'annotations/members'
OUTPUT_STATISTICS = 'reports/overall/resolution.json'


DEFAULT_ARGS = {
    'owner': 'dtkaczyk',
    'depends_on_past': False,
    'email': [get_secret('sampling-framework-mailto')],
    'email_on_failure': False,
    'email_on_retry': False,
}


def step_config(name, script, args):
    return {'Name': name,
            'ActionOnFailure': 'TERMINATE_CLUSTER',
            'HadoopJarStep': {
                'Jar': 'command-runner.jar',
                'Args': ['spark-submit',
                         '--deploy-mode',
                         'cluster',
                         script,
                         json.dumps(args)]}}

def breakdown_step_config(name, field):
    return step_config(name,
                       MEMBER_BREAKDOWN_SCRIPT,
                       {'date': DATE,
                        'input': LOGS_MEMBER_IDS,
                        'output-bucket': OUTPUT_BUCKET,
                        'output-prefix': OUTPUT_PREFIX,
                        'breakdown-field': field,
                        'output-field': field})

@dag(
    default_args=DEFAULT_ARGS,
    schedule_interval='@once',
    catchup=False,
    dagrun_timeout=timedelta(hours=10),
    start_date=datetime(2023, 4, 15),
    tags=['data analysis']
)
def resolution_logs():

    @task()
    def generate_prefix_map():
        prefixes = []
        offset = 0
        while True:
            members = requests.get(f'{API_URL}/members',
                                   {'rows': 1000, 'offset': offset}).json()['message']['items']
            if not members:
                break
            for m in members:
                for p in m['prefixes']:
                    prefixes.append(p + ' ' + str(m['id']))
            offset += 1000

        session = boto3.Session()
        s3 = session.resource('s3')
        obj = s3.Object(INPUT_BUCKET, PREFIXES_KEY)
        obj.put(Body='\n'.join(prefixes))
        return True

    @task()
    def start_spark_app(profixes_saved: bool):
        client = boto3.client('emr', region_name='us-east-1')
        response = client.run_job_flow(
            Name='resolution-logs',
            ReleaseLabel='emr-5.36.0',
            Applications=[{
                'Name': 'Spark'
            }],
            Instances={
                'MasterInstanceType': 'm5.xlarge',
                'SlaveInstanceType': 'm5.xlarge',
                'InstanceCount': 9,
                'KeepJobFlowAliveWhenNoSteps': False,
                'TerminationProtected': False
            },
            Steps=[step_config('resolution-logs-prepare',
                               PREPARE_LOGS_SCRIPT,
                               {'input': LOGS_INPUT,
                                'prefixes': PREFIXES,
                                'output': LOGS_MEMBER_IDS}),
                   step_config('resolution-logs-overall-statistics',
                               OVERALL_STATISTICS_SCRIPT,
                               {'date': DATE,
                                'input': LOGS_MEMBER_IDS,
                                'output-bucket': OUTPUT_BUCKET,
                                'output-file': OUTPUT_STATISTICS}),
                   step_config('resolution-logs-member-counts',
                               MEMBER_COUNTS_SCRIPT,
                               {'date': DATE,
                                'input': LOGS_MEMBER_IDS,
                                'output-bucket': OUTPUT_BUCKET,
                                'output-prefix': OUTPUT_PREFIX}),
                   breakdown_step_config('resolution-logs-member-dois', 'doi'),
                   breakdown_step_config('resolution-logs-member-ips', 'ip'),
                   breakdown_step_config('resolution-logs-member-full-domains', 'full-domain'),
                   breakdown_step_config('resolution-logs-member-registered-domains', 'registered-domain')
            ],
            BootstrapActions=[{
                'Name': 'Python packages',
                'ScriptBootstrapAction': {
                    'Path': BOOTSTRAP_SCRIPT
                }
            }],
            LogUri=get_secret('s3-url-logs'),
            VisibleToAllUsers=True,
            ServiceRole='EMR_DefaultRole',
            JobFlowRole='EMR_EC2_DefaultRole'
        )
        return response

    @task()
    def wait_for_spark(spark_response: dict):
        client = boto3.client('emr', region_name='us-east-1')
        waiter = client.get_waiter('cluster_terminated')
        waiter.wait(
            ClusterId=spark_response['JobFlowId'],
            WaiterConfig={
                'Delay': 60,
                'MaxAttempts': 600
            }
        )
        return True

    @task()
    def delete_prefix_map(calculated: bool):
        session = boto3.Session()
        s3 = session.resource('s3')
        obj = s3.Object(INPUT_BUCKET, PREFIXES_KEY)
        obj.delete()

    prefixes_saved = generate_prefix_map()
    spark_cluster = start_spark_app(prefixes_saved)
    calculated = wait_for_spark(spark_cluster)
    delete_prefix_map(calculated)

workflow = resolution_logs()

