import boto3
import json
import re
import sys

from pyspark.sql import functions
from pyspark.sql import SparkSession
from pyspark.sql.functions import count, col, row_number
from pyspark.sql.window import Window


CONFIG = json.loads(sys.argv[1])
DATE = CONFIG['date']
INPUT = CONFIG['input']
OUTPUT_BUCKET = CONFIG['output-bucket']
OUTPUT_PREFIX = CONFIG['output-prefix']

BREAKDOWN_FIELD = CONFIG['breakdown-field']
OUTPUT_FIELD = CONFIG['output-field']


def save_member_breakdown(member, breakdown):
    s3client = boto3.client('s3')
    key = f'{OUTPUT_PREFIX}/{member}/resolution.json'
    try:
        data = s3client.get_object(Bucket=OUTPUT_BUCKET, Key=key)['Body'].read().decode('utf-8')
        data = json.loads(data)
    except s3client.exceptions.NoSuchKey:
        data = []

    res_object = [o for o in data if o['about']['logs-collected-date'] == DATE]
    if res_object:
        res_object = res_object[0]
    else:
        res_object = {'about': {'logs-collected-date': DATE}}
        data.append(res_object)
    res_object['breakdowns'] = res_object.get('breakdowns', {})
    res_object['breakdowns'][OUTPUT_FIELD] = breakdown

    session = boto3.Session()
    s3 = session.resource('s3')
    obj = s3.Object(OUTPUT_BUCKET, key)
    obj.put(Body=json.dumps(data))
    return 1


spark = SparkSession.builder \
            .config('spark.jars.packages', 'org.apache.hadoop:hadoop-aws:2.8.5') \
            .getOrCreate()

logs = spark.read.option('mode', 'DROPMALFORMED').csv(INPUT)
logs = logs.na.fill('').na.fill(0)
logs = logs.toDF('ip', 'doi', 'prefix', 'member', 'full-domain', 'registered-domain')

member_stats = logs.groupBy('member', BREAKDOWN_FIELD).count()

window = Window.partitionBy('member').orderBy(col('count').desc())
member_stats = member_stats.withColumn('row', row_number().over(window)).filter(col('row') <= 10).drop('row').rdd
member_stats = member_stats.map(lambda x: (x[0], [{'value': x[1], 'count': x[2]}]))
member_stats = member_stats.reduceByKey(lambda a, b: a+b)
member_stats = member_stats.map(lambda s: save_member_breakdown(s[0], s[1]))
member_count = member_stats.sum()
