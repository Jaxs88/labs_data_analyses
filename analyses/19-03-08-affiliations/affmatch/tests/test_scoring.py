from affmatch.scoring import get_similarity, get_score


def test_get_similarity():
    assert get_similarity('University of Excellence',
                          'University of Excellence') == 100
    assert get_similarity('univ. of excellençë',
                          'Univërsity of Excellence') == 100
    assert get_similarity('of Excellence University',
                          'University of Excellence') == 100
    assert get_similarity('of excellençë univ',
                          'University of Excellence') == 100
    assert get_similarity('Excellence University',
                          'University of Excellence') == 93
    assert get_similarity('excellençë univ',
                          'University of Excellence') == 93
    assert get_similarity('excellençë', 'Excellence') == 100
    assert get_similarity('excellençë', 'Excellenc') == 0
    assert get_similarity('University of Exçellence',
                          'University of Excellence (Gallifrey)') == 100
    assert get_similarity('University of Excellence and Brilliance',
                          'University of Excellence') == 76
    assert get_similarity('University of Excellence (and Brilliance)',
                          'University of Excellence') == 100
    assert get_similarity('University of Excellence School of Perseverance',
                          'University of Excellence') == 100
    assert get_similarity('University of Excellence Mediocrity Hospital',
                          'University of Excellence') == 100


def test_get_score():
    empty = {'name': '',
             'labels': [],
             'aliases': [],
             'acronyms': [],
             'country': {'country_code': ''}}
    assert get_score(
        dict(empty, name='University of Excellence'),
        'University of Excellence', None) == 100
    assert get_score(
        dict(empty, name='University of Excellence',
             country={'country_code': 'XY'}),
        'University of Excellence', ['US-PR']) == 0
    assert get_score(
        dict(empty, name='University of Excellence',
             country={'country_code': 'PR'}),
        'University of Excellence', ['US-PR']) == 100
    assert get_score(
        dict(empty, labels=[{'label': 'University of Excellence'}]),
        'University of Excellence', None) == 100
    assert get_score(
        dict(empty, labels=[{'label': 'Excellence U'},
                            {'label': 'University of Excellence'}]),
        'University of Excellence', None) == 100
    assert get_score(
        dict(empty, aliases=['University of Excellence']),
        'University of Excellence', None) == 100
    assert get_score(
        dict(empty, aliases=['Excellence U', 'University of Excellence']),
        'University of Excellence', None) == 100
    assert get_score(dict(empty, acronyms=['UEXC']),
                     'University of Excellence', None) == 0
    assert get_score(dict(empty, acronyms=['UEXC']), 'UEXC', None) == 90
    assert get_score(
        dict(empty, acronyms=['UEXC'], country={'country_code': 'PR'}),
        'UEXC', ['US-PR']) == 100

    assert get_score(
        dict(empty, name='University of Excellence',
             labels=[{'label': 'Excellence U'},
                     {'label': 'University Excellence'}],
             aliases=['Excellence U', 'University Excellence'],
             acronyms=['UEXC']), 'University of Excellence', None) == 100
    assert get_score(
        dict(empty, name='University Excellence',
             labels=[{'label': 'Excellence U'},
                     {'label': 'University of Excellence'}],
             aliases=['Excellence U', 'University Excellence'],
             acronyms=['UEXC']), 'University of Excellence', None) == 100
    assert get_score(
        dict(empty, name='University Excellence',
             labels=[{'label': 'Excellence U'},
                     {'label': 'University Excellence'}],
             aliases=['Excellence U', 'University of Excellence'],
             acronyms=['UEXC']), 'University of Excellence', None) == 100
    assert get_score(
        dict(empty, name='University of Brilliance',
             labels=[{'label': 'University of Brilliance'}],
             aliases=['Brilliance U', 'University Brilliance'],
             acronyms=['UEXC']), 'UEXC', None) == 90
    assert get_score(
        dict(empty, name='University of Brilliance',
             labels=[{'label': 'University of Brilliance'}],
             aliases=['Brilliance U', 'University Brilliance'],
             acronyms=['UEXC'], country={'country_code': 'PR'}),
        'UEXC', ['US-PR']) == 100
    assert get_score(
        dict(empty, name='University of Brilliance',
             labels=[{'label': 'University of Brilliance'}],
             aliases=['Brilliance U', 'University Brilliance'],
             acronyms=['UEXC'], country={'country_code': 'AV'}),
        'UEXC', ['US-PR']) == 0
