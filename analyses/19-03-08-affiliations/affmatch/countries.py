import os
import re
import unidecode

from functools import lru_cache
from fuzzywuzzy import fuzz


@lru_cache(maxsize=None)
def load_countries():
    countries = []
    with open(os.path.join(os.path.split(__file__)[0],
                           'countries.txt')) as file:
        lines = [line.strip().split() for line in file]
        countries = [(line[0], ' '.join(line[1:])) for line in lines]
    return countries


COUNTRIES = load_countries()


def to_region(c):
    return {'GB': 'GB-UK', 'UK': 'GB-UK',
            'CN': 'CN-HK-TW', 'HK': 'CN-HK-TW', 'TW': 'CN-HK-TW',
            'PR': 'US-PR', 'US': 'US-PR'}.get(c, c)


def get_country_codes(string):
    string = unidecode.unidecode(string).strip()
    lower = re.sub('\s+', ' ', string.lower())
    lower_alpha = re.sub('\s+', ' ', re.sub('[^a-z]', ' ', string.lower()))
    alpha = re.sub('\s+', ' ', re.sub('[^a-zA-Z]', ' ', string))
    codes = []
    for code, name in COUNTRIES:
        if re.search('[^a-z]', name):
            score = fuzz.partial_ratio(name, lower)
        elif len(name) == 2:
            score = max([fuzz.ratio(name.upper(), t) for t in alpha.split()])
        else:
            score = max([fuzz.ratio(name, t) for t in lower_alpha.split()])
        if score >= 90:
            codes.append(code.upper())
    return list(set(codes))


def get_countries(string):
    codes = get_country_codes(string)
    return [to_region(c) for c in codes]
