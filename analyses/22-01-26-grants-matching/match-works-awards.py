import json
import random
import re
import requests
import time

from multiprocessing import Pool

mailto = '<EMAIL>'
url = 'https://api.crossref.org/'
date_filter = 'until-created-date:2022-03-08'

def iterate_items(path, filters=None):
    cursor = '*'
    while True:
        params = {'mailto': mailto, 'cursor': cursor, 'rows': 500, 'sort': 'indexed', 'order': 'asc'}
        if filters:
            params['filter'] = filters
        data = requests.get(url + path, params).json()['message']
        items = data['items']
        cursor = data['next-cursor']
        if not items:
            break
        for item in items:
            yield item

def get_items(path, filters=None):
    params = {'mailto': mailto, 'rows': 1000}
    if filters:
        params['filter'] = filters
    data = requests.get(url + path, params).json()['message']
    if data['total-results'] > 1000:
        return iterate_items(path, filters)
    items = data['items']
    for item in items:
        yield item


def funder_doi_norm(doi):
    if not doi:
        return ''
    return '10.13039/' + re.sub('.*\/', '', doi)

def grant_funder_dois(grant):
    return set([funder_doi_norm(fid['id']).lower()
                for project in grant['project']
                for funding in project['funding']
                for fid in funding['funder']['id']])


def are_aliases(funders, funder_dois_1, funder_dois_2):
    funder_dois_1 = [doi for doi in funder_dois_1 if doi and doi in funders]
    funder_dois_2 = [doi for doi in funder_dois_2 if doi and doi in funders]
    for doi_1 in funder_dois_1:
        for doi_2 in funder_dois_2:
            if doi_1 in funders[doi_2]['aliases'] or doi_2 in funders[doi_1]['aliases']:
                return True
    return False

def are_family(funders, funder_dois_1, funder_dois_2):
    funder_dois_1 = [doi for doi in funder_dois_1 if doi and doi in funders]
    funder_dois_2 = [doi for doi in funder_dois_2 if doi and doi in funders]
    for doi_1 in funder_dois_1:
        for doi_2 in funder_dois_2:
            if doi_1 in funders[doi_2]['descendants'] or doi_2 in funders[doi_1]['descendants']:
                return True
    return False

def name_matches(funders, funder_dois, funder_names, name='names'):
    for funder_doi in funder_dois:
        for funder_name in funder_names:
            if funder_name in funders[funder_doi][name]:
                return True
    return False


def candidate_relevant_awards(grant, candidate):
    for funder_object in candidate.get('funder', []):
        for award in funder_object.get('award', []):
            if award.lower() == grant['award'].lower():
                yield funder_object.get('DOI', '').lower(), funder_object.get('name', '').lower(), award.lower()
            if award.lower() == grant['DOI'].lower():
                yield funder_object.get('DOI', '').lower(), funder_object.get('name', '').lower(), award.lower()


def decide(grant, candidate, funders):
    if 'grant' == candidate['type']:
        return False, 'GRANT'

    grant_doi = grant['DOI'].lower()
    grant_award = grant['award'].lower()
    grant_funderdois = grant_funder_dois(grant)

    candidate_awards_data = list(candidate_relevant_awards(grant, candidate))
    candidate_funderdois = [a[0] for a in candidate_awards_data]
    candidate_fundernames = [a[1] for a in candidate_awards_data]
    candidate_awards = [a[2] for a in candidate_awards_data]

    if grant_doi in candidate_awards:
        return True, 'GRANT DOI AS AWARD'


    if [doi for doi in grant_funderdois if doi in candidate_funderdois]:
        return True, 'FUNDER DOI'

    if are_aliases(funders, grant_funderdois, candidate_funderdois):
        return True, 'FUNDER DOI ALIAS'
    
    if are_family(funders, grant_funderdois, candidate_funderdois):
        return True, 'FUNDER DOI FAMILY'

    candidate_fundernames_no_doi = [a[1] for a in candidate_awards_data if not a[0]]

    if name_matches(funders, grant_funderdois, candidate_fundernames_no_doi):
        return True, 'FUNDER NAME'

    if name_matches(funders, grant_funderdois, candidate_fundernames_no_doi, name='aliases-names'):
        return True, 'FUNDER NAME ALIAS'

    if name_matches(funders, grant_funderdois, candidate_fundernames_no_doi, name='family-names'):
        return True, 'FUNDER NAME FAMILY'

    return False, '?'

def process_grant(data):
    grant, funders = data
    time.sleep(5*random.random())
    grant_doi = grant['DOI'].lower()
    if 'award' not in grant:
        return []
    grant_award = grant['award'].lower()
    links = []
    for candidate in get_items('works', filters='award.number:'+grant_award+','+date_filter):
        decision, evidence = decide(grant, candidate, funders)
        links.append((grant_doi, candidate['DOI'], grant_award, evidence))
    for candidate in get_items('works', filters='award.number:'+grant_doi+','+date_filter):
        decision, evidence = decide(grant, candidate, funders)
        links.append((grant_doi, candidate['DOI'], grant_award, evidence))
    return links

def process(data):
    try:
        return process_grant(data)
    except:
        time.sleep(10)
        try:
            return process_grant(data)
        except:
            time.sleep(10)
            return process_grant(data)

if __name__ == '__main__':

    funders = {}
    with open('data/funders-data.jsonl', 'r') as f:
        for funder_data in f:
            funder = json.loads(funder_data)
            funders[funder['DOI']] = funder

    for funder in funders.values():
        for alias in funder['aliases']:
            if funder['DOI'] not in funders[alias]['aliases']:
                funders[alias]['aliases'].append(funder['DOI'])

    for funder in funders.values():
        ans = []
        for alias in funder['aliases']:
            ans.extend(funders[alias]['names'])
        funder['aliases-names'] = list(set(ans))

    for funder in funders.values():
        dns = []
        for desc in funder['descendants']:
            dns.extend(funders[desc]['names'])
        funder['family-names'] = list(set(dns))

    for funder in funders.values():
        for desc in funder['descendants']:
            funders[desc]['family-names'].extend(funder['names'])
            funders[desc]['family-names'] = list(set(funders[desc]['family-names']))

    grants = []
    for i, grant in enumerate(iterate_items('types/grant/works', filters=date_filter)):
        if len(grants) % 100 == 0:
            print(len(grants))
        grants.append(grant)

    summary = {}
    with Pool(6) as pool:
        results = pool.imap(process, [(g, funders) for g in grants])

        with open('data/award-links-1.csv', 'w') as f:
            for i, result in enumerate(results):
                print(i, summary)
                for grant_doi, candidate_doi, award, evidence in result:
                    if evidence == 'GRANT':
                        continue
                    f.write(grant_doi + ',' + candidate_doi + ',' + award + ',' + evidence + '\n')
                    if evidence not in summary:
                        summary[evidence] = 0
                    summary[evidence] += 1

