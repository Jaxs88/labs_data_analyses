import argparse
import utils.data_format_keys as dfk

from utils.utils import read_json


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='calculate the stats')
    parser.add_argument('-m', '--matched', type=str, required=True)

    args = parser.parse_args()

    references = read_json(args.matched)
    n = len(references)
    print('all references: {}'.format(n))

    matched = [r for r in references if dfk.CR_ITEM_DOI in r]
    print('Currently matched references: {} ({}%)'
          .format(len(matched), 100*len(matched)/n))

    matched_crossref = [r for r in references if dfk.CR_ITEM_DOI in r
                        and r['doi-asserted-by'] == 'crossref']
    print('Currently matched by Crossref: {} ({}%)'
          .format(len(matched_crossref), 100*len(matched_crossref)/n))

    matched_stq = [r for r in references if dfk.CR_ITEM_DOI not in r
                   and r['DOI_from_unstructured'] is not None]
    print('Additional matched references by Simple Text Query: {} ({}%)'
          .format(len(matched_stq), 100*len(matched_stq)/n))

    matched_ou = [r for r in references if dfk.CR_ITEM_DOI not in r
                  and r['DOI_from_structured'] is not None]
    print('Additional matched references by OpenURL: {} ({}%)'
          .format(len(matched_ou), 100*len(matched_ou)/n))

    matched_new = [r for r in references if dfk.CR_ITEM_DOI not in r
                   and (r['DOI_from_structured'] is not None
                        or r['DOI_from_unstructured'] is not None)]
    print('Additional matched references in total: {} ({}%)'
          .format(len(matched_new), 100*len(matched_new)/n))
